/**
* BASERT PÅ PROSJEKT 2
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

// Load the Polymer.Element base class and other dependencies 

import { PolymerElement } from '/imt2291-eksamen-v2019-coe/oppgave4/www/node_modules/@polymer/polymer/polymer-element.js';





import { setPassiveTouchGestures, setRootPath } from '/imt2291-eksamen-v2019-coe/oppgave4/www/node_modules/@polymer/polymer/lib/utils/settings.js';
import { html } from '/imt2291-eksamen-v2019-coe/oppgave4/www/node_modules/@polymer/polymer/lib/utils/html-tag.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(window.Polymer.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html      
`
<style>
    :host {
        --app-primary-color:  #d580ff;
        --app-secondary-color: black;
        display: block;
      }

</style>

<!-- Routing -->

<app-location route="{{route}}" url-space-regex="^[[rootPath]]"></app-location>
<app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}"></app-route>

<app-drawer-layout fullbleed="" narrow="{{narrow}}">



      <!-- Innhold fra headeren ( Endre overskrift i header f.eks) -->


            

    <div main-title="">Redigering av brukere</div>





     
`;
}

  static get is() { return 'my-app'; }

    static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      user: {
        type: Object,
      },
        
        
    };
  }


  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
      
    //  console.log('route changed');
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
      
      
    if (!page) {
      this.page = 'view1';
    } else if (['view1', ].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }
      

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    // Importer sidekomponentene on demand.
    //
    // Note: `polymer build` doesn't like string conenation in the import
    // statement, so break it up.
    switch (page) {
    case 'view1':
        import('./my-view1.js');
        break;
      
    }
  }
}



window.customElements.define('my-app', MyApp);
