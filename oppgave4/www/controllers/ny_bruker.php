<?php

require_once '../vendor/autoload.php';
require_once '../DB/user_reg.php';

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['fname'])) {
  echo $twig->render('nyBruker.html', array());
} else {
  $data['fname'] = $_POST['fname'];
  $data['lname'] = $_POST['lname'];
  $data['email'] = $_POST['email'];
  $data['password'] = $_POST['password1'];

  if($data['fname'] == ""){
      echo 'Fornavn er påkrevet';
  }
    else if($data['lname'] == ""){
        echo 'Etternavn er påkrevet';
    }
    else if($data['email'] == ""){
        echo 'E-post er påkrevet';
    }
    else if(strlen($data['password'] < 8)){
        echo 'Passord må inneholde minst 8 tegn';
    }
    else{
  $user = new User();
  $res = $user->brukerPluss($data);
  $res['data'] = $data;

  echo $twig->render('brukerPluss.html', $res);
    }
    
  

  
}