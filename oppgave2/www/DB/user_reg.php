<?php



class User{
    private $db;

    public function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost; dbname=myDb; charset=utf8','root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Error accured creating new PDO..";
            echo $e->getMessage();
        }
    }
    
    
    
    /**
     * Closes the DB when the object dies. 
     */
    public function __destruct() {
        if ($this->db!=null) {
          unset ($this->db);
        }
    }


    /**
     * Basert på /db/teacher og student_reg.php fra prosjekt 1
     * Men tilpasset oppgaven
     *
     * Registering of new user
     * @param: Data of array user which saves i DB
     * @return: array of data to check if a fault occured
     *  and to output the name that has been registered.
    **/
    public function brukerPluss($data){
        
        $sql = 'insert into user (firstName, lastName, pwd, uname) VALUES (?, ?, ?, ?)';
        $sth = $this->db->prepare ($sql);
        $pass = password_hash($data['password'], PASSWORD_DEFAULT);
        
        try {
            $sth->execute (array ($data['fname'], $data['lname'],
            $data['email'], $pass));
        
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert user into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        $tmp = [];
        if ($sth->rowCount()==1) {
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert user into contact registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }
}